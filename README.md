# Et si on jouait ? Nombre mystere !!

TP noté flutter, le jeu du nombre mystère avec plusieurs niveaux.

## Les grands principes :

La partie est lancée !
• Je rentre un nombre et l’application m’indique si je suis plus petit
ou plus grand que le nombre déterminé aléatoirement au préalable, et ainsi de suite…
• Une fois le nombre trouvé, je peux enregistrer mon score (nom + nombre de
tentatives)

Les niveaux:
• Niveau 1: 15 tentatives, intervalle de 0 à 50; nombres entiers
• Niveau 2: 13 tentatives, intervalle de -50 à 50; nombres entiers
• Niveau 3: 13 tentatives, intervalle de -50 à 50; nombres décimaux arrondis au dixième
• Niveau 4: 10 tentatives, intervalle de -100 à 100; nombres décimaux arrondis au dixième

Le score correspondra au nombre de tentatives sur le niveau, plus le score est, mieux c'est:
Chaque complétion de niveau débloque le prochain.
À la fin de chaque complétion de niveau le joueur peut enregistrer son nom avec son score, pour faire partie du classement.
Il serait bien de pouvoir voir le meilleur score par niveau ou le classement par niveau.

## Travail de groupe

La répartition du travail peut être décomposée en deux périodes. La première période qui correspond à la prise en main du projet était répartie de la manière suivante :
Roland RAKOTOMALALA :  chargé de déterminer la façon dont les données allaient être conservées.
Rebson Dodji DAHOUEDE : chargé de déterminer les règles du jeu puis de commencer l'interface graphique.

La deuxième période venant après que le projet aie été mis de côté; il fallait accélerer la cadence. 
Pour cela nous avons codé en liveShare pour avoir une collaboration plus dynamique où l'un pouvait avoir un visuel sur ce que faisait l'autre facilement étant donné qu'il n'y avait pas énormément de fichiers.



