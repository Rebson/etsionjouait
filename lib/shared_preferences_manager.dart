import 'package:shared_preferences/shared_preferences.dart';
import 'package:etsionjouait_nombremystere/scoredata.dart';

class SharedPreferencesManager {
  static saveScore(int level, ScoreData scoreData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = _getLevelKey(level);
    List<String> scores = prefs.getStringList(key) ?? [];
    scores.add('${scoreData.pseudo}:${scoreData.score}');
    await prefs.setStringList(key, scores);
  }

  static Future<List<ScoreData>> getScores(int level) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String key = _getLevelKey(level);
    List<String> scoreStrings = prefs.getStringList(key) ?? [];
    return scoreStrings.map((scoreString) {
      List<String> parts = scoreString.split(':');
      return ScoreData(parts[0], int.parse(parts[1]));
    }).toList();
  }

  static String _getLevelKey(int level) {
    switch (level) {
      case 1:
        return 'level1';
      case 2:
        return 'level2';
      case 3:
        return 'level3';
      case 4:
        return 'level4';
      default:
        throw ArgumentError('Invalid level: $level');
    }
  }
}
