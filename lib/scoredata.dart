class ScoreData {
  final String pseudo;
  final int score;

  ScoreData(this.pseudo, this.score);
}
