import 'package:etsionjouait_nombremystere/home.dart';
import 'package:etsionjouait_nombremystere/jouer.dart';
import 'package:etsionjouait_nombremystere/regle_du_jeu.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'historique.dart';

final GlobalKey<NavigatorState> _goRouterKey = GlobalKey<NavigatorState>();

class AppRouter  {

  AppRouter();

  get router => _router;

  late final _router = GoRouter(
    initialLocation: '/',
    navigatorKey: _goRouterKey,
    routes: [
      GoRoute(
        path: '/',
        builder: (context, state) {
          return const Home(title: 'Et Si On Jouait au nombre mystère ?');
        },
      ),
      GoRoute(
        path: '/regle',
        builder: (context, state) {
          return Regle();
        },
      ),
      GoRoute(
        path: '/jouer',
        builder: (context, state) {
          return Jeu();
        },
      ),
      GoRoute(
        path: '/classement',
        builder: (context, state) {
          return Classement();
        },
      ),
    ],
  );
}