import 'package:etsionjouait_nombremystere/scoredata.dart';
import 'package:etsionjouait_nombremystere/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'dart:math';

class Jeu extends StatefulWidget {

  @override
  _JeuState createState() => _JeuState();
}

class _JeuState extends State<Jeu> {
  int _level = 1; // Niveau initial
  dynamic _minValue = 0; // Valeur minimale par défaut pour le niveau 1
  dynamic _maxValue = 50; // Valeur maximale par défaut pour le niveau 1
  dynamic _randomNumber = 0; // Nombre mystère généré aléatoirement
  bool _isNumberFound = false; // Indique si le nombre a été trouvé ou non
  int _attempts = 0; // Nombre de tentatives effectuées par le joueur
  int _maxAttempts = 15; // Nombre maximal de tentatives pour le niveau 1
  int _attemptTemporary = 0;
  int _levelTemporary = 1;
  final Random _random = Random();
  String _pseudo = '';
  TextEditingController _controller = TextEditingController();


  @override
  void initState() {
    super.initState();
    _generateRandomNumber();
  }

  void _askForPseudo() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Entrez votre pseudo'),
          content: TextField(
            onChanged: (value) {
              setState(() {
                _pseudo = value; // Mettre à jour le pseudo lorsque l'utilisateur tape
              });
            },
            decoration: InputDecoration(hintText: 'Pseudo'),
          ),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop(); // Fermer la boîte de dialogue
              },
              child: Text('Valider'),
            ),
          ],
        );
      },
    );
  }

  void _generateRandomNumber() {
    switch (_level) {
      case 1:
        _randomNumber = _minValue + _random.nextInt(51); // Génère un nombre entre 0 et 50 inclus
        break;
      case 2:
        _randomNumber = _minValue + _random.nextInt(101); // Génère un nombre entre -50 et 50 inclus
        break;
      case 3:
        _randomNumber = _minValue + (_random.nextDouble() * ((_maxValue - _minValue) * 10 + 1)) / 10; // Génère un nombre décimal entre -50 et 50 avec une précision d'un dixième
        _randomNumber = double.parse(_randomNumber.toStringAsFixed(1)); // Arrondi au dixième
        break;
      case 4:
        _randomNumber = _minValue + (_random.nextDouble() * ((_maxValue - _minValue) * 10 + 1)) / 10; // Génère un nombre décimal entre -100 et 100 avec une précision d'un dixième
        _randomNumber = double.parse(_randomNumber.toStringAsFixed(1)); // Arrondi au dixième
        break;
      default:
        break;
    }
  }

  void _checkNumber(String value) {
    dynamic enteredNumber;
    if (value.contains('.')) {
      enteredNumber = double.tryParse(value) ?? 0.0; // Parse avec double si le string contient un "."
    } else {
      enteredNumber = int.tryParse(value) ?? 0; // Parse avec int sinon
    }

    if (_level == 1 && _attempts ==0){
      _askForPseudo();
    }

    if (!_isNumberFound && _attempts < _maxAttempts) {
      setState(() {
        _attempts++;
        _attemptTemporary = _attempts;
        _levelTemporary = _level;
        // Mettre à jour les valeurs minimale et maximale en fonction de la proposition du joueur
        if (enteredNumber == _randomNumber && _level <= 4) {
          // Le nombre a été trouvé, passez au niveau suivant
          _showSaveScoreDialog();
          _isNumberFound = true;
          _level++;
          _attempts = 0;
          switch (_level) {
            case 2:
              _isNumberFound = false;
              _minValue = -50;
              _maxValue = 50;
              _maxAttempts = 13;
              break;
            case 3:
              _isNumberFound = false;
              _minValue = -50;
              _maxValue = 50;
              _maxAttempts = 13;
              // Ajoutez ici les extrémités pour le niveau 3
              break;
            case 4:
              _isNumberFound = false;
              _minValue = -100;
              _maxValue = 100;
              _maxAttempts = 10;
              // Ajoutez ici les extrémités pour le niveau 4
              break;
            default:
              break;
          }
          _generateRandomNumber();
        } else {
          if (enteredNumber < _randomNumber && enteredNumber > _minValue) {
            _minValue = enteredNumber ;
          } else if (enteredNumber > _randomNumber && enteredNumber < _maxValue){
            _maxValue = enteredNumber ;
          }
        }
      });
    } else {
      GoRouter.of(context).go('/');
    }
  }

  void _showSaveScoreDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Enregistrer le score'),
          content: Text('Voulez-vous enregistrer votre score ?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Fermer la boîte de dialogue
                _saveScore(); // Enregistrer le score
              },
              child: Text('Oui'),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Fermer la boîte de dialogue
              },
              child: Text('Non'),
            ),
          ],
        );
      },
    );
  }

  void _saveScore() async {
    // Enregistrer le score en utilisant SharedPreferencesManager
    await SharedPreferencesManager.saveScore(_levelTemporary, ScoreData(_pseudo, _attemptTemporary));
    // Continuer la partie
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Niveau $_level'), // Affiche le niveau actuel
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Tentatives max $_maxAttempts - Compteur: $_attempts',
              style: const TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              'Trouvez le nombre mystère entre $_minValue et $_maxValue : $_randomNumber',
              style: const TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              'Pseudo: $_pseudo',
              style: const TextStyle(
                fontSize: 16.0,
              ),
            ),
            const SizedBox(height: 20),
            if (_level == 1)
              TextField(
                controller: _controller,
                keyboardType: const TextInputType.numberWithOptions(signed: false, decimal: false),
                decoration: const InputDecoration(
                  hintText: 'Entrez votre nombre',
                ),
              ),
            if (_level == 2)
              TextField(
                controller: _controller,
                keyboardType: const TextInputType.numberWithOptions(signed: true, decimal: false),
                decoration: const InputDecoration(
                  hintText: 'Entrez votre nombre',
                ),
              ),
            if (_level > 2)
              TextField(
                controller: _controller,
                keyboardType: const TextInputType.numberWithOptions(signed: true, decimal: true),
                decoration: const InputDecoration(
                  hintText: 'Entrez votre nombre',
                ),
              ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                _checkNumber(_controller.text);
              },
              child: const Text('Valider'),
            ),
          ],
        ),
      ),
    );
  }
}
