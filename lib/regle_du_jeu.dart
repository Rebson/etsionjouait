import 'package:flutter/material.dart';

class Regle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Règles du jeu'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary
      ),
      backgroundColor: Theme.of(context).colorScheme.inverseSurface ,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Règles :',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white70,
              ),
            ),
            const SizedBox(height: 10.0),
            const Text(
              'La partie est lancée !\n'
                  '• Je rentre un nombre et l’application m’indique si je suis plus petit '
                  'ou plus grand que le nombre déterminé aléatoirement au préalable, et ainsi de suite…\n'
                  '• Une fois le nombre trouvé, je peux enregistrer mon score (nom + nombre de tentatives)',
              style: TextStyle(
                color: Colors.white70,
              ),
            ),
            const SizedBox(height: 20.0),
            const Text(
              'Les niveaux :',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white70,
              ),
            ),
            const SizedBox(height: 10.0),
            _buildNiveau('Niveau 1', '15 tentatives, intervalle de 0 à 50; nombres entiers'),
            _buildNiveau('Niveau 2', '13 tentatives, intervalle de -50 à 50; nombres entiers'),
            _buildNiveau('Niveau 3', '13 tentatives, intervalle de -50 à 50; nombres décimaux arrondis au dixième'),
            _buildNiveau('Niveau 4', '10 tentatives, intervalle de -100 à 100; nombres décimaux arrondis au dixième'),
            const SizedBox(height: 20.0),
            const Text(
              'Le score :',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.white70,
              ),
            ),
            const SizedBox(height: 10.0),
            const Text(
              'Le score correspondra au nombre de tentatives sur le niveau, plus le score est bas, mieux c\'est.\n'
                  'Chaque complétion de niveau débloque le prochain.\n'
                  'À la fin de chaque complétion de niveau, le joueur peut enregistrer son nom avec son score, pour faire partie du classement.\n'
                  'Il serait bien de pouvoir voir le meilleur score par niveau ou le classement par niveau.',
              style: TextStyle(
                color: Colors.white70,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildNiveau(String titre, String description) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          titre,
          style: const TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            color: Colors.white70,
          ),
        ),
        const SizedBox(height: 5.0),
        Text(
          description,
          style: const TextStyle(
            color: Colors.white70,
          ),
        ),
        const SizedBox(height: 10.0),
      ],
    );
  }
}
