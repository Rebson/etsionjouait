import 'package:etsionjouait_nombremystere/router.dart';
import 'package:flutter/material.dart';
import 'package:etsionjouait_nombremystere/home.dart';
import 'package:go_router/go_router.dart';

class AppMystere extends StatelessWidget {
  const AppMystere({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Et Si On Jouait au nombre mystère ?',
      theme: ThemeData(colorScheme: ColorScheme.fromSeed(seedColor: Colors.red)),
      routerConfig: AppRouter().router, // Utilisation du router
    );
  }
}
