import 'package:flutter/material.dart';
import 'package:etsionjouait_nombremystere/scoredata.dart';
import 'package:etsionjouait_nombremystere/shared_preferences_manager.dart';

class Classement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Classement des scores par niveau'),
      ),
      body: FutureBuilder<Map<int, List<ScoreData>>>(
        future: _getScores(),
        builder: (BuildContext context, AsyncSnapshot<Map<int, List<ScoreData>>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (snapshot.hasError) {
              return Center(
                child: Text('Une erreur s\'est produite : ${snapshot.error}'),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  int level = snapshot.data!.keys.elementAt(index);
                  List<ScoreData> scores = snapshot.data![level]!;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Niveau $level',
                          style: const TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: scores.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            title: Text(scores[index].pseudo),
                            subtitle: Text('Tentatives: ${scores[index].score}'),
                          );
                        },
                      ),
                    ],
                  );
                },
              );
            }
          }
        },
      ),
    );
  }

  Future<Map<int, List<ScoreData>>> _getScores() async {
    Map<int, List<ScoreData>> scoresByLevel = {};
    for (int i = 1; i <= 4; i++) {
      List<ScoreData> scores = await SharedPreferencesManager.getScores(i);
      scoresByLevel[i] = scores;
    }
    return scoresByLevel;
  }
}
